const React = require('react');
const moment = require('moment');

/**
 * Render edit/delete buttons and post timestamp.
 */
const PostMeta = ({ post, time, onEdit, onDelete }) => (
  <div className="blog-post-meta">
    <a role="button" title="Edit post"
      style={{ paddingRight: '8px' }}
      onClick={ onEdit }
    >
      <span className="fa fa-edit" />
    </a>

     {/* TODO Task 7: Add a delete button */}

    { moment(post.createdAt).from(time.now) }
  </div>
);

/**
 * A read-only view of a blog post.
 * This is a pure functional component.
 * It takes props as its args and returns what the render method would return.
  */
  // Add code for delete
const PostView = ({ post, time, onEdit, onDelete }) => (
  <div className="blog-post">
    <h2 className="blog-post-title">{post.title}</h2>

    {/* TODO Task 3: Display blog metadata */}


    {/* TODO Task 3: Display blog content */}

  </div>
);

module.exports = PostView;
