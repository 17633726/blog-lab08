const React = require('react');
const PostEdit = require('./PostEdit');

/**
 * A button which expands into a form for writing a new post.
 */
const PostNew = React.createClass({
  displayName: 'PostNew',
  getInitialState: function() {
    return { editing: false };
  },
  openEdit: function() {
    this.setState({ editing: true });
  },
  closeEdit: function() {
    this.setState({ editing: false });
  },
  createPost: function(newPost) {
    this.props.createPost(newPost, (err) => {
      if(!err) this.closeEdit();
    });
  },
  render: function() {
    // TODO Task 6: Write code to switch to edit mode when editing is clicked.

      return (
      <button className="blog-load-more btn btn-primary btn-lg"
        onClick={ this.openEdit }
      >
        Write new post
      </button>
    );
  }
});

module.exports = PostNew;
